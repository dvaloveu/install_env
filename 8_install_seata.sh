#!/bin/bash

if [ "$EUID" -ne 0 ]; then 
    echo "请以 root 权限运行此脚本"
    exit 1
fi

# set -e

# 检查是否提供了 JSON 配置文件
if [ ! -f "install_config.json" ]; then
    echo "找不到配置文件 install_config.json"    
    exit 1
fi

# 读取配置文件中的变量
SEATA_DIR=$(grep -o '"seata_dir": *"[^"]*"' install_config.json | sed 's/"seata_dir": *"\([^"]*\)"/\1/')
SEATA_TAR=$(grep -o '"seata_tar": *"[^"]*"' install_config.json | sed 's/"seata_tar": *"\([^"]*\)"/\1/')
MYSQL_PASSWORD=$(grep -o '"mysql_new_password": *"[^"]*"' install_config.json | sed 's/"mysql_new_password": *"\([^"]*\)"/\1/')
MYSQL_DRIVER=$(grep -o '"mysql_driver": *"[^"]*"' install_config.json | sed 's/"mysql_driver": *"\([^"]*\)"/\1/')
JDK_DIR=$(grep -o '"jdk_dir": *"[^"]*"' install_config.json | sed 's/"jdk_dir": *"\([^"]*\)"/\1/')
SEA_MYSQL_URL=$(grep -o '"seata_mysql_ip": *"[^"]*"' install_config.json | sed 's/"seata_mysql_ip": *"\([^"]*\)"/\1/')
SEA_REDIS_HOST=$(grep -o '"seata_redis_host": *"[^"]*"' install_config.json | sed 's/"seata_redis_host": *"\([^"]*\)"/\1/')
SEA_REDIS_PORT=$(grep -o '"seata_redis_port": *"[^"]*"' install_config.json | sed 's/"seata_redis_port": *"\([^"]*\)"/\1/')
REDIS_PASS=$(grep -o '"redis_password": *"[^"]*"' install_config.json | sed 's/"redis_password": *"\([^"]*\)"/\1/')
SEA_NACOS_IP=$(grep -o '"seata_nacos_ip": *"[^"]*"' install_config.json | sed 's/"seata_nacos_ip": *"\([^"]*\)"/\1/')
MYSQL_DIR=$(grep -o '"mysql_dir": *"[^"]*"' install_config.json | sed 's/"mysql_dir": *"\([^"]*\)"/\1/')
SEATA_HOST_IP=$(grep -o '"seata_host_ip": *"[^"]*"' install_config.json | sed 's/"seata_host_ip": *"\([^"]*\)"/\1/')


# 解压安装包到安装目录
mkdir -p "$SEATA_DIR"
tar -zxvf "$SEATA_TAR" -C "$SEATA_DIR" --strip-components=1

# 生成启动脚本到安装目录
cat <<EOF > $SEATA_DIR/seata_start_1.sh
#!/bin/bash
# 切换到 seata 安装路径
cd $SEATA_DIR/bin

# 执行 seata 启动脚本
sudo sh seata-server.sh -p 8091 -h $SEATA_HOST_IP -m db
EOF

chmod +x "$SEATA_DIR/seata_start_1.sh"


# 复制 JDBC 驱动到 Seata 的 lib/jdbc 目录下，删除旧版本的驱动
cp $MYSQL_DRIVER -f "$SEATA_DIR/lib/jdbc/"
rm "$SEATA_DIR/lib/jdbc/mysql-connector-java-8.0.19.jar"

# 写入配置文件到配置目录
cat <<EOF > $SEATA_DIR/conf/file.conf
## transaction log store, only used in seata-server
store {
  ## store mode: file、db、redis
  mode = "db"

  ## file store property
  file {
    ## store location dir
    dir = "sessionStore"
    # branch session size , if exceeded first try compress lockkey, still exceeded throws exceptions
    maxBranchSessionSize = 16384
    # globe session size , if exceeded throws exceptions
    maxGlobalSessionSize = 512
    # file buffer size , if exceeded allocate new buffer
    fileWriteBufferCacheSize = 16384
    # when recover batch read size
    sessionReloadReadSize = 100
    # async, sync
    flushDiskMode = async
  }

  ## database store property
  db {
    ## the implement of javax.sql.DataSource, such as DruidDataSource(druid)/BasicDataSource(dbcp)/HikariDataSource(hikari) etc.
    datasource = "druid"
    ## mysql/oracle/postgresql/h2/oceanbase etc.
    dbType = "mysql"
    driverClassName = "com.mysql.cj.jdbc.Driver"
    url = "jdbc:mysql://$SEA_MYSQL_URL/seata?useUnicode=true&characterEncoding=UTF-8&useSSL=false&serverTimezone=Asia/Shanghai"
    user = "root"
    password = "$MYSQL_PASSWORD"
    minConn = 5
    maxConn = 30
    globalTable = "global_table"
    branchTable = "branch_table"
    lockTable = "lock_table"
    queryLimit = 100
    maxWait = 5000
  }

  ## redis store property
  redis {
    host = "$SEA_REDIS_HOST"
    port = "$SEA_REDIS_PORT"
    password = "$REDIS_PASS"
    database = "0"
    minConn = 1
    maxConn = 10
    queryLimit = 100
  }

}
EOF


cat <<EOF > $SEATA_DIR/conf/registry.conf
registry {
  type = "nacos"
  nacos {
    application = "seata-server"
    serverAddr = "$SEA_NACOS_IP"
    group = "SEATA_GROUP"
    namespace = ""
    cluster = "default"
    username = ""
    password = ""
  }
}
config {
  type = "file"
  file {
    name = "file:$SEATA_DIR/conf/file.conf"
  }
}

EOF


# 检查 seata_mysql.sql 文件是否存在
if [ -f "seata_mysql.sql" ]; then
    echo "seata_mysql.sql 文件存在于当前目录。"
else
    echo "seata_mysql.sql 文件不存在于当前目录。"
fi

# 创建并导入数据库
# 检查数据库是否存在
DB_EXISTS=$("$MYSQL_DIR/bin/mysql" -u "root" -p"$MYSQL_PASSWORD" -e "SHOW DATABASES LIKE 'seata';" | grep "seata")

# 如果数据库存在，则删除
if [ -n "$DB_EXISTS" ]; then
    echo "数据库 seata 已存在。正在删除..."
    "$MYSQL_DIR/bin/mysql" -u "root" -p"$MYSQL_PASSWORD" -e "DROP DATABASE seata;"
    echo "数据库 seata 删除成功。"
fi

# 创建数据库
echo "创建数据库 seata..."
"$MYSQL_DIR/bin/mysql" -u "root" -p"$MYSQL_PASSWORD" -e "CREATE DATABASE IF NOT EXISTS seata DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;"

# 检查数据库是否创建成功
if [ $? -eq 0 ]; then
    echo "数据库 seata 创建成功."
else
    echo "创建数据库失败 seata."
    exit 1
fi

# 登录 MySQL 并执行 SQL 文件
"$MYSQL_DIR/bin/mysql" -u "root" -p"$MYSQL_PASSWORD" seata < seata_mysql.sql

# 检查执行结果
if [ $? -eq 0 ]; then
    echo "SQL 文件执行成功。"
else
    echo "SQL 文件执行失败。"
fi

# 创建java软连接                        
# 软链接路径
LINK_PATH="/usr/bin/java"

# 检查是否存在软链接
if [ -L "$LINK_PATH" ]; then
    # 存在软链接，删除它
    sudo rm -f "$LINK_PATH"
    echo "已删除旧软链接 $LINK_PATH"
else
    echo "旧软链接 $LINK_PATH 不存在"
fi

# 创建新的软链接
sudo ln -s "$JDK_DIR/bin/java" "$LINK_PATH"
echo "已创建新软链接 $LINK_PATH -> $JDK_DIR/bin/java"
# https://blog.csdn.net/a807719447/article/details/103604470

# 创建 Seata 服务
SERVICE_FILE="/etc/systemd/system/seata.service"
cat <<EOF | sudo tee "$SERVICE_FILE" >/dev/null
[Unit]
Description=Seata Server
After=syslog.target network.target remote-fs.target nss-lookup.target 


[Service]
Type=simple
ExecStart=$SEATA_DIR/seata_start_1.sh
WorkingDirectory=$SEATA_DIR
TimeoutStartSec=0
RestartSec=2
Restart=always

[Install]
WantedBy=multi-user.target
EOF

# Environment="JAVA_PATH=$JDK_DIR/bin:$JDK_DIR/jre/bin"

# 启动服务
sudo systemctl daemon-reload
sudo systemctl enable seata
sudo systemctl start seata


# 开放8091tcp端口
firewall-cmd --permanent --add-port=8091/tcp
firewall-cmd --reload
echo "防火墙8091/tcp端口已开放"

sleep 3
# 检查端口
sudo netstat -tulpn | grep :8091
echo "Seata服务已安装并启动。"
echo "使用以下命令测试安装成功"
echo "sudo ps aux|grep seata"
echo "sudo netstat -tulpn | grep :8091"
echo "访问nacos web页面查看seata服务"
