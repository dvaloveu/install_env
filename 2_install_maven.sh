#!/bin/bash

# 确保脚本以 root 权限运行
if [ "$EUID" -ne 0 ]; then 
    echo "请以 root 权限运行此脚本"
    exit 1
fi

# 停止脚本在出现任何错误时
set -e

# 检查是否提供了 JSON 配置文件
if [ ! -f "install_config.json" ]; then
    echo "找不到配置文件 install_config.json"
    exit 1
fi

# 读取 JSON 配置文件
MAVEN_DIR=$(grep -o '"maven_dir": *"[^"]*"' install_config.json | sed 's/"maven_dir": *"\([^"]*\)"/\1/')
MAVEN_TAR=$(grep -o '"maven_tar": *"[^"]*"' install_config.json | sed 's/"maven_tar": *"\([^"]*\)"/\1/')

# 安装 Maven
echo "安装 Maven ..."
echo "Maven 目录：$MAVEN_DIR"
echo "Maven 压缩包：$MAVEN_TAR"

# 检查 Maven 压缩包是否存在
if [ ! -f "$MAVEN_TAR" ]; then
    echo "Maven 压缩包未找到: $MAVEN_TAR"
    exit 1
fi

# 解压 Maven 压缩包到指定目录
echo "正在解压 Maven ..."
mkdir -p "$MAVEN_DIR"
tar -zxvf "$MAVEN_TAR" -C "$MAVEN_DIR" --strip-components=1

# 更新 /etc/profile 文件以设置环境变量
echo "正在更新环境变量 ..."
echo "export MAVEN_HOME=$MAVEN_DIR" >> /etc/profile
echo 'export PATH=$PATH:$MAVEN_HOME/bin' >> /etc/profile


source /etc/profile
echo "使用以下命令手动更新环境变量"
echo "source /etc/profile"
echo "Maven 安装和配置完成。使用 mvn -v  测试maven "
