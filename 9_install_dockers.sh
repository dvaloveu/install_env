if [ "$EUID" -ne 0 ]; then 
    echo "请以 root 权限运行此脚本"
    exit 1
fi

set -e

# 检查是否提供了 JSON 配置文件
if [ ! -f "install_config.json" ]; then
    echo "找不到配置文件 install_config.json"    
    exit 1
fi

#读取配置文件
DOCKER_WORK_DIR=$(grep -o '"docker_work_dir": *"[^"]*"' install_config.json | sed 's/"docker_work_dir": *"\([^"]*\)"/\1/')
DOCKER_FASTDFS_IP=$(grep -o '"docker_fastdfs_ip": *"[^"]*"' install_config.json | sed 's/"docker_fastdfs_ip": *"\([^"]*\)"/\1/')

if command -v docker &> /dev/null
then
    # 如果安装了Docker，则卸载它
    echo "检测到 Docker 已安装。正在卸载 Docker..."
    # 停止并删除所有运行的容器
    # sudo docker stop $(sudo docker ps -a -q)
    # sudo docker rm $(sudo docker ps -a -q)

    # 卸载Docker引擎
    sudo echo y |  yum remove docker-ce docker-ce-cli containerd.io

    # 删除Docker创建的数据和配置文件
    sudo rm -rf /var/lib/docker
    sudo rm -rf /etc/docker
    sudo rm -rf $DOCKER_WORK_DIR
    
    echo "Docker 已成功卸载。"
else
    echo "未安装 Docker。"
fi

# 安装docker本体----------------------------------------------
# 使用官方安装脚本自动安装最新版
curl -fsSL https://get.docker.com | bash -s docker --mirror Aliyun  


# 更新↑↑↑自动配置的 dockerservice 文件
# # 定义service文件的路径  
# SERVICE_FILE="/usr/lib/systemd/system/docker.service"  
  
# # 检查文件是否存在  
# if [ ! -f "$SERVICE_FILE" ]; then  
#     echo "错误: $SERVICE_FILE 文件不存在."  
#     exit 1  
# fi  
  
# # 使用sed命令在Requires指令后面添加seata.service  
# sed -i '/^Requires=/s/$/ seata.service/' "$SERVICE_FILE"  
  
# # 检查sed命令是否成功修改了文件  
# if [ $? -ne 0 ]; then  
#     echo "错误: 无法修改 $SERVICE_FILE 文件."  
#     exit 1  
# fi  
  
# 重新加载systemd配置  
systemctl daemon-reload  
  

# 配置 Docker 日志大小和保存数量
# 编辑 /etc/docker/daemon.json 文件进行配置
cat <<EOF > /etc/docker/daemon.json
{
  "log-driver": "json-file",
  "log-opts": {
    "max-size": "10m",
    "max-file": "10"
  },
  "data-root": "$DOCKER_WORK_DIR"
}
EOF

# 启动 Docker 服务  //源安装会自动配置systemctl
systemctl stop docker 
systemctl enable docker 
systemctl start docker 

# # 查看 Docker 版本信息
# docker version

# 查看 Docker 相关信息
docker info




# 创建docker工作目录，用于放置dockerfile文件
mkdir -p "$DOCKER_WORK_DIR"

# 安装RABBITMQ 最新版并启动------------------------------------------------

docker run -d -p 15672:15672 -p 5672:5672 -e RABBITMQ_DEFAULT_VHOST=my_vhost -e RABBITMQ_DEFAULT_USER=admin -e RABBITMQ_DEFAULT_PASS=admin --restart=always --name rabbitmq rabbitmq:management

# 安装fastdfs最新版并启动------------------------------------------------------------------
mkdir -p /home/fastdfs:/var/local/fdfs
docker run -d --restart=always --privileged=true --net=host --name=fastdfs -e IP="$DOCKER_FASTDFS_IP" -e WEB_PORT=8089 -v /home/fastdfs:/var/local/fdfs registry.cn-beijing.aliyuncs.com/tianzuo/fastdfs

sudo docker version


# 开放 15672,5672，8089 tcp端口
firewall-cmd --permanent --add-port=15672/tcp
firewall-cmd --permanent --add-port=5672/tcp
firewall-cmd --permanent --add-port=8089/tcp
firewall-cmd --reload

echo "防火墙15672,5672，808/tcp端口已开放"

echo "docker从阿里云镜像最新版安装完成"
echo "使用以下命令测试docker"
echo "sudo ps aux|grep docker"
echo "sudo docker version"
echo "使用以下命令测试RABBITMQ"
echo "sudo netstat -tulpn | grep :15672"
echo "使用以下命令测试fastdfs"
echo "sudo netstat -tulpn | grep :8089"
sudo netstat -tulpn | grep :15672
sudo netstat -tulpn | grep :8089
