#!/bin/bash  

# 根据内网IP地址生成配置文件2024.4.16

# 检查参数是否存在
if [ $# -ne 2 ]; then
    echo "用法： $0 123456 123456 "
    exit 1
fi


set -e

# 获取本地内网地址
local_ip=$(ip route get 1 | awk '{print $NF;exit}')

# 输出内网地址
echo "本地内网地址: $local_ip"

# 定义 IP 地址变量  
IP_ADDRESS=$local_ip  # 将这里的 YOUR_DESIRED_IP_ADDRESS 替换为您想要的 IP 地址  
  
# 创建一个临时文件来构建 JSON 配置  
TMP_FILE=$(mktemp)  
  
# 将 JSON 配置写入临时文件  
sudo tee "$TMP_FILE" >/dev/null <<EOF
{  
    "java": {  
        "jdk_dir": "/home/work/java/jdk1.8",  
        "jdk_tar": "jdk-8u231-linux-x64.tar.gz"  
    },  
    "maven": {  
        "maven_dir": "/home/work/apache-maven",  
        "maven_tar": "apache-maven-3.6.3-bin.tar.gz"  
    },  
    "docker_compose": {  
        "dockercompose_dir": "/usr/local/bin/",  
        "dockercompose_tar": "docker-compose"  
    },  
    "nginx": {  
        "nginx_dir": "/home/work/nginx",  
        "nginx_tar": "nginx-1.20.1.tar.gz"  
    },  
    "redis": {  
        "redis_dir": "/home/work/redis",  
        "redis_tar": "redis-6.0.6.tar.gz",  
        "redis_bind_ip": "${IP_ADDRESS}",  
        "redis_password": "$1"  
    },  
    "mysql": {  
        "mysql_dir": "/home/work/mysql",  
        "mysql_tar_xz": "mysql-8.0.20-linux-glibc2.12-x86_64.tar.xz",  
        "mysql_new_password": "$2",  
        "mysql_config_file": "/etc/my.cnf"  
    },  
    "nacos": {  
        "nacos_dir": "/home/work/nacos",  
        "nacos_tar_gz": "nacos-server-1.2.1.tar.gz",  
        "mysql_driver": "mysql-connector-java-8.0.20.jar",  
        "nacos_mysql_ip": "${IP_ADDRESS}:3306",  
        "spring.redis.host": "${IP_ADDRESS}",  
        "spring.redis.port": "6379"  
    },  
    "seata": {  
        "seata_dir": "/home/work/seata",  
        "seata_tar": "seata-server-1.3.0.tar.gz",  
        "seata_host_ip": "${IP_ADDRESS}",  
        "seata_mysql_ip": "${IP_ADDRESS}:3306",  
        "seata_redis_host": "${IP_ADDRESS}",  
        "seata_redis_port": "6379",  
        "seata_nacos_ip": "${IP_ADDRESS}:8848"  
    },  
    "docker": {  
        "docker_work_dir": "/home/work/dockerapps",  
        "docker_fastdfs_ip": "${IP_ADDRESS}"  
    }  
}
EOF
  
# 将临时文件的内容复制到最终配置文件，强制覆盖  
mv  "$TMP_FILE" ./install_config.json  
  
# 打印成功信息并退出脚本  
echo "Configuration file generated with IP address: $IP_ADDRESS"  
exit 0
