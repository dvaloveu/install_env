#!/bin/bash

# 确保脚本以 root 权限运行
if [ "$EUID" -ne 0 ]; then 
    echo "请以 root 权限运行此脚本"
    exit 1
fi

# 停止脚本在出现任何错误时
set -e

# 检查是否提供了 JSON 配置文件
if [ ! -f "install_config.json" ]; then
    echo "找不到配置文件 install_config.json"
    exit 1
fi

# 读取 JSON 配置文件
DOCKER_COMPOSE_DIR=$(grep -o '"dockercompose_dir": *"[^"]*"' install_config.json | sed 's/"dockercompose_dir": *"\([^"]*\)"/\1/')
DOCKER_COMPOSE_TAR=$(grep -o '"dockercompose_tar": *"[^"]*"' install_config.json | sed 's/"dockercompose_tar": *"\([^"]*\)"/\1/')

# 安装 Docker Compose
echo "安装 Docker Compose ..."
echo "Docker Compose 安装目录：$DOCKER_COMPOSE_DIR"
echo "Docker Compose 安装包：$DOCKER_COMPOSE_TAR"

# 检查 Docker Compose 安装包是否存在
if [ ! -f "$DOCKER_COMPOSE_TAR" ]; then
    echo "Docker Compose 安装包未找到: $DOCKER_COMPOSE_TAR"
    exit 1
fi

# 复制 Docker Compose 安装包到指定目录
echo "正在设置 Docker Compose ..."
cp -f "$DOCKER_COMPOSE_TAR" "$DOCKER_COMPOSE_DIR"
chmod +x "${DOCKER_COMPOSE_DIR}${DOCKER_COMPOSE_TAR}"
echo "Docker Compose 设置完成，使用以下命令测试"
echo "docker-compose -version"

