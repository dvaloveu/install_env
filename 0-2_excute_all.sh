#!/bin/bash
if [ "$EUID" -ne 0 ]; then 
    echo "请以 root 权限运行此脚本"
    exit 1
fi

set -e # 报错就停下

# 检查是否提供了 JSON 配置文件
if [ ! -f "install_config.json" ]; then
    echo "找不到配置文件 install_config.json"    
    exit 1
fi

for script in {1..9}_*; do
    echo "Executing sudo sh $script"
    sudo sh "$script" <<< "y"
done

