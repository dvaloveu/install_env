#!/bin/bash

# 确保脚本以 root 权限运行
if [ "$EUID" -ne 0 ]; then 
    echo "请以 root 权限运行此脚本"
    exit 1
fi

# 停止脚本在出现任何错误时
set -e

# 检查是否提供了 JSON 配置文件
if [ ! -f "install_config.json" ]; then
    echo "找不到配置文件 install_config.json"
    exit 1
fi

# 读取 JSON 配置文件
JDK_DIR=$(grep -o '"jdk_dir": *"[^"]*"' install_config.json | sed 's/"jdk_dir": *"\([^"]*\)"/\1/')
JDK_TAR=$(grep -o '"jdk_tar": *"[^"]*"' install_config.json | sed 's/"jdk_tar": *"\([^"]*\)"/\1/')


# 安装 JDK
echo "安装 JDK ..."
echo "JDK 目录：$JDK_DIR"
echo "JDK 压缩包：$JDK_TAR"

# 检查 JDK 压缩包是否存在
if [ ! -f "$JDK_TAR" ]; then
    echo "JDK 压缩包未找到: $JDK_TAR"
    exit 1
fi

# 解压 JDK 压缩包到指定目录
echo "正在解压 JDK ..."
mkdir -p "$JDK_DIR"
tar -zxvf "$JDK_TAR" -C "$JDK_DIR" --strip-components=1

# 更新 /etc/profile 文件以设置环境变量
echo "正在更新环境变量 ..."
echo "export JAVA_HOME=$JDK_DIR" >> /etc/profile
echo 'export JRE_HOME=${JAVA_HOME}/jre' >> /etc/profile
echo 'export CLASSPATH=.:${JAVA_HOME}/lib:${JRE_HOME}/lib:$CLASSPATH' >> /etc/profile
echo 'export JAVA_PATH=${JAVA_HOME}/bin:${JRE_HOME}/bin' >> /etc/profile
echo 'export PATH=$PATH:${JAVA_PATH}' >> /etc/profile


source /etc/profile
echo "JDK 安装和配置完成"
echo "使用以下命令手动更新环境变量"
echo "source /etc/profile"
echo "使用以下命令测试安装成功"
echo "javac -version"
