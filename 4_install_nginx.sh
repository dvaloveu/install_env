#!/bin/bash

# 确保脚本以 root 权限运行
if [ "$EUID" -ne 0 ]; then 
    echo "请以 root 权限运行此脚本"
    exit 1
fi

# 停止脚本在出现任何错误时
set -e

# 安装依赖
echo "安装Nginx依赖..."
yum install -y gcc-c++
yum install -y pcre pcre-devel
yum install -y zlib zlib-devel
yum install -y openssl openssl-devel
echo "所有Nginx依赖安装完毕！"

# 检查是否提供了 JSON 配置文件
if [ ! -f "install_config.json" ]; then
    echo "找不到配置文件 install_config.json"
    exit 1
fi

# 读取 JSON 配置文件
NGINX_DIR=$(grep -o '"nginx_dir": *"[^"]*"' install_config.json | sed 's/"nginx_dir": *"\([^"]*\)"/\1/')
NGINX_TAR=$(grep -o '"nginx_tar": *"[^"]*"' install_config.json | sed 's/"nginx_tar": *"\([^"]*\)"/\1/')

# 确认 Nginx 压缩包存在
if [ ! -f "$NGINX_TAR" ]; then
    echo "Nginx 压缩包不存在: $NGINX_TAR"
    exit 1
fi

# 解压 Nginx 压缩包
echo "正在解压 Nginx ..."
mkdir -p "$NGINX_DIR"
mkdir -p "$NGINX_DIR/nginx_extract"
tar zxvf "$NGINX_TAR" -C "$NGINX_DIR/nginx_extract" --strip-components=1

# 配置安装路径并编译 Nginx
cd "$NGINX_DIR/nginx_extract"
echo "配置安装路径并编译 Nginx ..."
./configure --prefix="$NGINX_DIR"
make
# 安装 Nginx
echo "安装 Nginx ..."
sudo make install

# 创建 systemd 服务文件
echo "创建 systemd 服务文件 ..."
cat > /etc/systemd/system/nginx.service << EOF
[Unit]
Description=The NGINX HTTP and reverse proxy server
After=network.target

[Service]
Type=forking
ExecStart=$NGINX_DIR/sbin/nginx
ExecReload=$NGINX_DIR/sbin/nginx -s reload
ExecStop=$NGINX_DIR/sbin/nginx -s quit
PrivateTmp=true
TimeoutStartSec=0
RestartSec=2
Restart=always

[Install]
WantedBy=multi-user.target
EOF

# 重新加载 systemd，启用并启动 Nginx
echo "重新加载 systemd..."
systemctl daemon-reload
echo "启用 Nginx 服务..."
systemctl enable nginx
echo "启动 Nginx 服务..."
systemctl start nginx

# 开放80tcp端口
firewall-cmd --permanent --add-port=80/tcp
firewall-cmd --reload
echo "防火墙80/tcp端口已开放"

echo "Nginx 已配置为系统命令并设置为开机启动"
echo "使用以下命令手动更新环境变量"
echo "source /etc/profile"
echo "使用以下命令测试安装成功"
echo "sudo netstat -tulpn | grep :80"
echo "访问IP地址:80"
echo "管理服务指令"
echo "sudo systemctl start/stop/status nginx"