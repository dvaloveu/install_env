#!/bin/bash  
  
# 定义脚本需要的变量  
SERVICE_NAME="restart_ims"  
SERVICE_UNIT_FILE="/etc/systemd/system/${SERVICE_NAME}.service"  
SCRIPT_PATH="$HOME/restart_ims.sh"  
USERNAME="root"  

# 检查文件是否存在
if [ ! -f "$SCRIPT_PATH" ]; then
    echo "找不到启动容器脚本 $SCRIPT_PATH"    
    exit 1
fi
  
# 创建服务单元文件  180秒后执行docker重启命令
cat > "$SERVICE_UNIT_FILE" <<EOF
[Unit]  
Description=Restart IMS at boot  
Requires=docker.service  

[Service]
Type=simple  
ExecStart=${SCRIPT_PATH} 180
StandardOutput=append:$HOME/Restart_ims.log
StandardError=append:$HOME/Restart_ims.log
Restart=on-failure  
RestartSec=10 

[Install]  
WantedBy=multi-user.target  
EOF
  
# 重新加载systemd配置  
sudo systemctl daemon-reload  
  
# 启用并启动服务  
sudo systemctl enable "${SERVICE_NAME}.service"  
sudo systemctl start "${SERVICE_NAME}.service"  
  
# 检查服务状态  
sudo systemctl status "${SERVICE_NAME}.service"  
  
echo "Service ${SERVICE_NAME} has been set up and started."