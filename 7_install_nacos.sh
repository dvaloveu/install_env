# 确保脚本以 root 权限运行
if [ "$EUID" -ne 0 ]; then 
    echo "请以 root 权限运行此脚本"
    exit 1
fi

# 停止脚本在出现任何错误时
# set -e

# 检查是否提供了 JSON 配置文件
if [ ! -f "install_config.json" ]; then
    echo "找不到配置文件 install_config.json"
    exit 1
fi

# 读取 Nacos 配置文件中的变量
NACOS_DIR=$(grep -o '"nacos_dir": *"[^"]*"' install_config.json | sed 's/"nacos_dir": *"\([^"]*\)"/\1/')
NACOS_TAR_GZ=$(grep -o '"nacos_tar_gz": *"[^"]*"' install_config.json | sed 's/"nacos_tar_gz": *"\([^"]*\)"/\1/')
MYSQL_DRIVER=$(grep -o '"mysql_driver": *"[^"]*"' install_config.json | sed 's/"mysql_driver": *"\([^"]*\)"/\1/')
MYSQL_URL=$(grep -o '"nacos_mysql_ip": *"[^"]*"' install_config.json | sed 's/"nacos_mysql_ip": *"\([^"]*\)"/\1/')
MYSQL_PASSWORD=$(grep -o '"mysql_new_password": *"[^"]*"' install_config.json | sed 's/"mysql_new_password": *"\([^"]*\)"/\1/')
REDIS_PASS=$(grep -o '"redis_password": *"[^"]*"' install_config.json | sed 's/"redis_password": *"\([^"]*\)"/\1/')
REDIS_HOST=$(grep -o '"spring_redis_host": *"[^"]*"' install_config.json | sed 's/"spring_redis_host": *"\([^"]*\)"/\1/')
REDIS_PORT=$(grep -o '"spring_redis_port": *"[^"]*"' install_config.json | sed 's/"spring_redis_port": *"\([^"]*\)"/\1/')
JDK_DIR=$(grep -o '"jdk_dir": *"[^"]*"' install_config.json | sed 's/"jdk_dir": *"\([^"]*\)"/\1/')
MYSQL_DIR=$(grep -o '"mysql_dir": *"[^"]*"' install_config.json | sed 's/"mysql_dir": *"\([^"]*\)"/\1/')



# 检查 Nacos .tar.gz 压缩包是否存在
if [ ! -f "$NACOS_TAR_GZ" ]; then
    echo "Nacos .tar.gz 压缩包不存在于当前目录: $NACOS_TAR_GZ"
    exit 1
fi

# 解压 Nacos 服务器到目标目录
mkdir -p "$NACOS_DIR"
tar -zxvf "$NACOS_TAR_GZ" --strip-components=1 -C "$NACOS_DIR"

# 创建 MySQL 插件目录
mkdir -p "$NACOS_DIR/plugins/mysql"

# 复制 MySQL 驱动到 Nacos 插件目录
if [ ! -f "$MYSQL_DRIVER" ]; then
    echo "MySQL 驱动不存在于当前目录: $MYSQL_DRIVER"
    exit 1
else
    cp "$MYSQL_DRIVER" -f "$NACOS_DIR/plugins/mysql/" 
fi

# 修改 Nacos 的 application.properties 文件
echo "spring.datasource.platform=mysql" >> "$NACOS_DIR/conf/application.properties"
echo "db.num=1" >> "$NACOS_DIR/conf/application.properties"
echo "db.url.0=jdbc:mysql://$MYSQL_URL/nacos_config?characterEncoding=utf8&connectTimeout=1000&socketTimeout=3000&autoReconnect=true&useUnicode=true&useSSL=false&serverTimezone=UTC" >> "$NACOS_DIR/conf/application.properties"
echo "db.user=root" >> "$NACOS_DIR/conf/application.properties"
echo "db.password=$MYSQL_PASSWORD" >> "$NACOS_DIR/conf/application.properties"

# 创建 Redis 配置并写入
echo "spring.redis.host=$REDIS_HOST" >> "$NACOS_DIR/conf/application.properties"
echo "spring.redis.port=$REDIS_PORT" >> "$NACOS_DIR/conf/application.properties"
echo "spring.redis.password=$REDIS_PASS" >> "$NACOS_DIR/conf/application.properties"
echo "spring.redis.database=0" >> "$NACOS_DIR/conf/application.properties"



# 创建Nacos数据库
# MySQL 信息
MYSQL_DB="nacos_config"
MYSQL_SQL_FILE="$NACOS_DIR/conf/nacos-mysql.sql"

# 检查数据库是否存在
DB_EXISTS=$("$MYSQL_DIR/bin/mysql" -u "root" -p"$MYSQL_PASSWORD" -e "SHOW DATABASES LIKE '$MYSQL_DB';" | grep "$MYSQL_DB")

# 如果数据库存在，则删除
if [ -n "$DB_EXISTS" ]; then
    echo "数据库 $MYSQL_DB 已存在。正在删除..."
    "$MYSQL_DIR/bin/mysql" -u "$MYSQL_USER" -p"$MYSQL_PASSWORD" -e "DROP DATABASE $MYSQL_DB;"
    echo "数据库 $MYSQL_DB 删除成功。"
fi

# 创建数据库
echo "创建数据库 $MYSQL_DB..."
"$MYSQL_DIR/bin/mysql" -u "$MYSQL_USER" -p"$MYSQL_PASSWORD" -e "CREATE DATABASE IF NOT EXISTS $MYSQL_DB DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;"

# 检查数据库是否创建成功
if [ $? -eq 0 ]; then
    echo "数据库 $MYSQL_DB 创建成功."
else
    echo "创建数据库失败 $MYSQL_DB."
    exit 1
fi

# 登录 MySQL 并执行 SQL 文件
"$MYSQL_DIR/bin/mysql" -u "$MYSQL_USER" -p"$MYSQL_PASSWORD" "$MYSQL_DB" < "$MYSQL_SQL_FILE"

# 检查执行结果
if [ $? -eq 0 ]; then
    echo "Nacos SQL 文件执行成功。"
else
    echo "Nacos SQL 文件执行失败。"      
fi

# 创建 Nacos Systemd 服务文件
NACOS_SERVICE_FILE="/etc/systemd/system/nacos.service"

# 开放8848tcp端口
firewall-cmd --permanent --add-port=8848/tcp
firewall-cmd --reload
echo "防火墙8848/tcp端口已开放"



# 使用超级用户权限创建和编辑服务文件
cat <<EOF | sudo tee "$NACOS_SERVICE_FILE"
[Unit]
Description=Nacos Server
After=network.target 


[Service]
Type=forking
Environment="JAVA_HOME=$JDK_DIR"
Environment="JRE_HOME=$JDK_DIR/jre" 
Environment="CLASSPATH=.:$JDK_DIR/lib:$JDK_DIR/jre/lib" 

ExecStart="$NACOS_DIR/bin/startup.sh" -m standalone
ExecStop="$NACOS_DIR/bin/shutdown.sh"
TimeoutStartSec=0
RestartSec=2
Restart=always

[Install]
WantedBy=multi-user.target
EOF

# Environment="JAVA_PATH=$JDK_DIR/bin:$JDK_DIR/jre/bin"

# 重载 Systemd 守护进程，启用和启动 Nacos 服务
sudo systemctl daemon-reload
sudo systemctl enable nacos
sudo systemctl start nacos
sudo netstat -tulpn | grep :8848
echo "Nacos服务已安装并启动。"
echo "使用以下命令测试安装成功"
echo "sudo ps aux|grep nacos"
echo "sudo netstat -tulpn | grep :8848"
echo "web访问 ip:8848/nacos"





