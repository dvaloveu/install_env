#!/bin/bash

echo "注意：编译安装 Redis 可能会占用大量内存，请确保系统有足够的可用内存！！！！！！！！！！！！！！！！！！！！！！"
echo "如果编译安装失败，请重启并释放内存！！！！！！！！！！！！！！"

# 等待用户输入yes或y
read -p "请确认是否继续(yes/y/no): " confirm
if [ "$confirm" != "yes" ] && [ "$confirm" != "y" ]; then
    echo "用户取消操作"
    exit 1
fi

# 确保脚本以 root 权限运行
if [ "$EUID" -ne 0 ]; then 
    echo "请以 root 权限运行此脚本"
    exit 1
fi

# 停止脚本在出现任何错误时
set -e

# 安装依赖
echo "安装Redis依赖..."
yum install -y centos-release-scl
yum install -y devtoolset-9-gcc devtoolset-9-gcc-c++ devtoolset-9-binutils

# 检查 devtoolset-9 启用脚本是否存在
DEVTOOLSET_ENABLE_SCRIPT="/opt/rh/devtoolset-9/enable"
if [ -f "$DEVTOOLSET_ENABLE_SCRIPT" ]; then
    # 向 /etc/profile 添加启用命令
    if grep -q "$DEVTOOLSET_ENABLE_SCRIPT" /etc/profile; then
        echo "devtoolset-9 已经在 /etc/profile 中启用"
    else
        echo "source $DEVTOOLSET_ENABLE_SCRIPT" >> /etc/profile
        echo "已添加 devtoolset-9 到 /etc/profile"
    fi
else
    echo "devtoolset-9 启用脚本不存在，请确认 devtoolset-9 已正确安装"
    exit 1
fi

source /etc/profile

echo "Redis 依赖安装完毕"

# 检查是否提供了 JSON 配置文件
if [ ! -f "install_config.json" ]; then
    echo "找不到配置文件 install_config.json"
    exit 1
fi

# 读取 JSON 配置文件
REDIS_DIR=$(grep -o '"redis_dir": *"[^"]*"' install_config.json | sed 's/"redis_dir": *"\([^"]*\)"/\1/')
REDIS_TAR=$(grep -o '"redis_tar": *"[^"]*"' install_config.json | sed 's/"redis_tar": *"\([^"]*\)"/\1/')
REDIS_BIND_IP=$(grep -o '"redis_bind_ip": *"[^"]*"' install_config.json | sed 's/"redis_bind_ip": *"\([^"]*\)"/\1/')
REDIS_PASS=$(grep -o '"redis_password": *"[^"]*"' install_config.json | sed 's/"redis_password": *"\([^"]*\)"/\1/')

# 确认 Redis 压缩包存在
if [ ! -f "$REDIS_TAR" ]; then
    echo "Redis 压缩包不存在与当前目录: $REDIS_TAR"
    exit 1
fi

# 解压 Redis 包并将文件解压到目标目录
echo "正在解压 Redis ..."
mkdir -p "$REDIS_DIR"
tar -zxvf "$REDIS_TAR" -C "$REDIS_DIR" --strip-components=1


# 编译安装redis
cd "$REDIS_DIR"
sudo make install


# 更改 Redis 配置文件
echo "修改 Redis 配置文件 ..."
sed -i "s/^bind 127.0.0.1/bind $REDIS_BIND_IP/" redis.conf   #绑定ip配置在config.json里
sed -i "s/^# requirepass .*/requirepass $REDIS_PASS/" redis.conf
sed -i "s/^protected-mode yes/protected-mode no/" redis.conf
# sed -i 's/^daemonize no/daemonize yes/' redis.conf
# 如果您使用的是守护进程模式（daemonize yes），请改为 no，因为 systemd 会处理后台运行

# 设置 Redis 可执行文件和配置文件的路径
REDIS_EXEC="$REDIS_DIR/src/redis-server"
REDIS_CLI="$REDIS_DIR/src/redis-cli"
REDIS_CONF="$REDIS_DIR/redis.conf"



# 创建 Systemd 服务文件
echo "创建 systemd 服务文件 ..."
cat > /etc/systemd/system/redis.service << EOF
[Unit]
Description=Redis In-Memory Data Store
After=network.target 


[Service]
ExecStart=$REDIS_EXEC $REDIS_CONF
ExecStop=$REDIS_CLI shutdown
TimeoutStartSec=0
RestartSec=2
Restart=always

[Install]
WantedBy=multi-user.target
EOF

# 重新加载 systemd，启用并启动 Redis 服务
echo "重新加载 systemd..."
systemctl daemon-reload
echo "启用 Redis 服务..."
systemctl enable redis
echo "启动 Redis 服务..."
systemctl start redis

# 开放6379tcp端口
firewall-cmd --permanent --add-port=6379/tcp
firewall-cmd --reload
echo "防火墙6379/tcp端口已开放"


echo "Redis 服务已安装并启动"

netstat -tuln | grep 6379
sudo ps aux | grep redis-server
# redis-cli ping
echo "使用以下命令测试安装成功"
echo "sudo netstat -tuln | grep 6379"
echo "sudo ps aux | grep redis-server"





