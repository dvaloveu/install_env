#!/bin/bash  
  
# 定义执行间隔的默认值  
DEFAULT_EXCUTE_INTERVAL=4  
  
# 定义要生成的脚本文件名  
GENERATED_SCRIPT="$HOME/restart_ims.sh"  
  
# 初始化一个空文件来存储生成的脚本  
> "$GENERATED_SCRIPT"  
  
# 使用EOF开始生成脚本内容，并将内容追加到GENERATED_SCRIPT文件中  
cat >> "$GENERATED_SCRIPT" <<EOF
#!/bin/bash  
  
# 检查参数是否存在  
if [ \$# -ne 1 ]; then  
    echo "延迟描述执行脚本内容，用法： \$0 秒数"  
    echo "单位sec"  
    exit 1  
fi  
  
echo "\$(date) - 等待 \$1 后重启docker中各个容器" >> "$HOME/Restart_ims.log"

# 初始的sleep命令，延迟执行重启docker容器的秒数
sleep \$1  

echo "\$(date) - 开始重启容器，每隔4秒启动一个容器" >> "$HOME/Restart_ims.log"

EOF
  
# 获取所有正在运行的Docker容器的名称  
running_services=$(docker ps --format "{{.Names}}")  
  
# 遍历正在运行的Docker服务  
for service in $running_services; do  
    # # 检查服务名称是否以ims开头  
    # if [[ $service == ims* ]]; then  
        # 向生成的脚本中添加重启命令和延迟  
        echo "sudo docker restart $service" >> "$GENERATED_SCRIPT"  
        echo "sleep $DEFAULT_EXCUTE_INTERVAL" >> "$GENERATED_SCRIPT"  
    # fi  
done  

cat >> "$GENERATED_SCRIPT" <<EOF
echo "\$(date) - 所有容器依次启动完毕" >> "$HOME/Restart_ims.log"
EOF

chmod +x $GENERATED_SCRIPT
  
# 输出提示信息  
echo "Generated script: $GENERATED_SCRIPT"  
echo "You can now run: sh $GENERATED_SCRIPT <seconds>"