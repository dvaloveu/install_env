#!/bin/bash

if [ "$EUID" -ne 0 ]; then 
    echo "请以 root 权限运行此脚本"
    exit 1
fi

# 检查是否提供了 JSON 配置文件
if [ ! -f "install_config.json" ]; then
    echo "找不到配置文件 install_config.json"
    exit 1
fi

# 读取 MySQL 配置文件中的变量
MYSQL_DIR=$(grep -o '"mysql_dir": *"[^"]*"' install_config.json | sed 's/"mysql_dir": *"\([^"]*\)"/\1/')
MYSQL_TAR_XZ=$(grep -o '"mysql_tar_xz": *"[^"]*"' install_config.json | sed 's/"mysql_tar_xz": *"\([^"]*\)"/\1/')
MYSQL_NEW_PASSWORD=$(grep -o '"mysql_new_password": *"[^"]*"' install_config.json | sed 's/"mysql_new_password": *"\([^"]*\)"/\1/')
MYSQL_CONFIG_FILE=$(grep -o '"mysql_config_file": *"[^"]*"' install_config.json | sed 's/"mysql_config_file": *"\([^"]*\)"/\1/')

# 安装必要的依赖
yum install -y libaio-devel numactl


# 检查 .tar.xz 压缩包是否存在
if [ ! -f "$MYSQL_TAR_XZ" ]; then
    echo "MySQL .tar.xz 压缩包不存在于当前目录: $MYSQL_TAR_XZ"
    exit 1
fi

# 解压 .tar.xz 文件到目标目录
# tar -xvf "$MYSQL_TAR_XZ" -C "$MYSQL_DIR" ----strip-components=1
mkdir -p "$MYSQL_DIR"
tar -xvf "$MYSQL_TAR_XZ" --strip-components=1 -C "$MYSQL_DIR"

# 检查 mysql 用户组是否存在，如果不存在则创建
if ! grep -q '^mysql:' /etc/group; then
    groupadd mysql
fi

# 检查 mysql 用户是否存在，如果不存在则创建
if ! id -u mysql &>/dev/null; then
    useradd -r -g mysql -s /bin/false mysql
fi

# 创建 mysql 的 data 文件夹并赋予权限
mkdir -p "$MYSQL_DIR/data"
chown -R mysql:mysql "$MYSQL_DIR/data"


# 以root无密码初始化mysql
cd "$MYSQL_DIR/bin"
./mysqld --user=mysql --lower-case-table-names=1 --initialize-insecure --basedir=$MYSQL_DIR --datadir=$MYSQL_DIR/data
# 注：--lower-case-table-names=1 该定义了mysql大小写模糊，初始化以后修改my.cnf无效启动mysql会报错。

# 设置环境变量
echo "export PATH=\$PATH:$MYSQL_DIR/bin" >> /etc/profile

# 配置 my.cnf
# 2024.4.29, 追加mysql配置，join缓存改为16M
cat > $MYSQL_CONFIG_FILE <<EOF
[mysqld]
port=3306
join_buffer_size=16M
basedir=$MYSQL_DIR
datadir=$MYSQL_DIR/data
skip-name-resolve
max_connections=1000
character-set-server=utf8
default-storage-engine=INNODB
max_allowed_packet=16M
default-authentication-plugin=mysql_native_password
log-error=$MYSQL_DIR/data/error.log
pid-file=$MYSQL_DIR/data/mysql.pid
lower-case-table-names=1
# Disabling symbolic-links is recommended to prevent assorted security risks
symbolic-links=0
# Settings user and group are ignored when systemd is used.
# If you need to run mysqld under a different user or group,
# customize your systemd unit file for mariadb according to the
# instructions in http://fedoraproject.org/wiki/Systemd

[mysql]
default-character-set=utf8
EOF


# 创建 Systemd 服务文件
cat > /etc/systemd/system/mysql.service <<EOF
[Unit]
Description=MySQL Server
After=network.target 


[Service]
User=mysql
Group=mysql
ExecStart=$MYSQL_DIR/bin/mysqld --defaults-file=$MYSQL_CONFIG_FILE
ExecStop=$MYSQL_DIR/bin/mysqladmin shutdown
TimeoutStartSec=0
RestartSec=2
Restart=on-failure

[Install]
WantedBy=multi-user.target
EOF

# 重新加载 systemd 并启用服务
systemctl daemon-reload
systemctl enable mysql

# 启动 MySQL 服务
systemctl start mysql

# 等待 MySQL 启动
echo "等待mysql启动"
sleep 5

source /etc/profile

# 登录到 MySQL
"$MYSQL_DIR/bin/mysql" -u root << EOF
-- 设置新密码并允许远程登录，
ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY '$MYSQL_NEW_PASSWORD';
ALTER USER 'root'@'localhost' PASSWORD EXPIRE NEVER;

USE mysql;
UPDATE user SET host='%' WHERE user='root';
FLUSH PRIVILEGES;  
EOF

echo "MySQL root 密码已配置，并开启远程访问。"

# 开放3306tcp端口
firewall-cmd --permanent --add-port=3306/tcp
firewall-cmd --reload
echo "防火墙3306/tcp端口已开放"



echo "MySQL 安装和配置完成。"

sudo ps aux|grep mysql
echo "使用以下命令手动更新环境变量"
echo "source /etc/profile"
echo "使用以下命令测试安装成功"
echo "sudo ps aux|grep mysql"
echo "sudo netstat -tulpn | grep :3306"
